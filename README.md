# IntelliSense TestProject

Microservice Backend Test Project

## Technology Stack

- OpenJDK 18.0.1
- Spring Boot 2.7.1
- Junit 5.9
- Open API 1.6.9
- Swagger UI

### URL

```
http://localhost:8080/api/v1/periods
```

### Docker Hub
```
https://hub.docker.com/r/parametheus/testproject
```

## Deploy API service
### Run image from Docker Hub

```
docker run -p 8080:8080 parametheus/testproject:1.0
```

## Try the service

Using a web browser access the following URL

```
http://localhost:8080/swagger-ui/index.html
```

Steps
- Then click on the green box (POST /api/v1/periods)
- Then click on (Try it out) button.
- At this point, you will be able to modify the Request Body and edit the period parameter with the desired value.
- Finally, click the (Execute) button and scroll down to see the Response.
