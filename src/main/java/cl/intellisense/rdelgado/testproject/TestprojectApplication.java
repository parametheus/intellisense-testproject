package cl.intellisense.rdelgado.testproject;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Microservice Backend Test Project", version = "1.0", description = "Richard Delgado"))
public class TestprojectApplication {
    public static void main(String[] args) {
        SpringApplication.run(TestprojectApplication.class, args);
    }
}
