package cl.intellisense.rdelgado.testproject.controller;

import cl.intellisense.rdelgado.testproject.payload.PeriodDto;
import cl.intellisense.rdelgado.testproject.service.PeriodService;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/periods")
public class PeriodController {
    private PeriodService periodService;

    public PeriodController(PeriodService periodService) {
        this.periodService = periodService;
    }

    /**
     * Retrieves the averages of the data series grouped by the desired period
     * @param periodDto the period
     * @return the JSON response
     */
    @PostMapping
    public ObjectNode getPeriods(@Valid @RequestBody PeriodDto periodDto) {
        return periodService.getPeriods(periodDto);
    }
}
