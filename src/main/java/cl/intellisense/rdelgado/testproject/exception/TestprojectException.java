package cl.intellisense.rdelgado.testproject.exception;
import org.springframework.http.HttpStatus;

public class TestprojectException  extends RuntimeException {

    private HttpStatus status;
    private String message;

    public TestprojectException(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
    }

    public TestprojectException(String message, HttpStatus status, String message1) {
        super(message);
        this.status = status;
        this.message = message1;
    }

    public HttpStatus getStatus() {
        return status;
    }

    @Override
    public String getMessage() {
        return message;
    }
}

