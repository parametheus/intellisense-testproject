package cl.intellisense.rdelgado.testproject.constraints;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

/**
 * Implement OneOf validation
 */
public class OneOfValidator implements ConstraintValidator<OneOf, Integer> {

    int[] allowedValues;

    @Override
    public void initialize(OneOf oneOf) {
        allowedValues = oneOf.value();
    }

    @Override
    public boolean isValid(Integer integer, ConstraintValidatorContext constraintValidatorContext) {
        return Arrays.stream(allowedValues).anyMatch(i -> i == integer);
    }
}
