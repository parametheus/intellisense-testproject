package cl.intellisense.rdelgado.testproject.payload;

import cl.intellisense.rdelgado.testproject.constraints.OneOf;

import javax.validation.constraints.NotNull;

public class PeriodDto {
    @NotNull(message = "Period must not be null")
    @OneOf(value = {10, 30, 60}, message = "Period must match one of the values: 10, 30, 60 minutes")
    private int period;

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }
}
