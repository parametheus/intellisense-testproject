package cl.intellisense.rdelgado.testproject.service.impl;

import cl.intellisense.rdelgado.testproject.payload.PeriodDto;
import cl.intellisense.rdelgado.testproject.service.DataProviderService;
import cl.intellisense.rdelgado.testproject.service.PeriodService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Service
public class PeriodServiceImpl implements PeriodService {

    @Autowired
    ObjectMapper objectMapper;

    private DataProviderService dataProviderService;

    public PeriodServiceImpl(DataProviderService dataProviderService) {
        this.dataProviderService = dataProviderService;
    }

    @Override
    public ObjectNode getPeriods(PeriodDto periodDto) {
        int period = periodDto.getPeriod();
        JsonNode dataset = dataProviderService.convertToJsonNode(dataProviderService.getDataset());

        ObjectNode mainNode = new ObjectNode(JsonNodeFactory.instance);
        ObjectNode childNode = new ObjectNode(JsonNodeFactory.instance);

        for (Iterator<Map.Entry<String, JsonNode>> rootNode = dataset.fields(); rootNode.hasNext(); ) {
            Map.Entry<String, JsonNode> dataGroup = rootNode.next();

            for (Iterator<Map.Entry<String, JsonNode>> it = dataGroup.getValue().fields(); it.hasNext(); ) {
                Map.Entry<String, JsonNode> elt = it.next();

                if (!"time".equals(elt.getKey())) {
                    serialDataController(childNode, period, elt.getKey(), elt.getValue());
                } else {
                    timeDataController(childNode, period, elt.getKey(), elt.getValue());
                }
            }

            mainNode.set(dataGroup.getKey(), childNode);
        }

        return mainNode;
    }

    private void serialDataController(ObjectNode childNode, int period, String serialName, JsonNode serialData) {
        int counter = 1;
        List<BigDecimal> dd = new ArrayList<>();
        BigDecimal sum = BigDecimal.ZERO;

        for (final JsonNode data : serialData) {

            if (!data.toString().equals("null")) {
                sum = sum.add(new BigDecimal(data.toString()));
            }

            if (counter == period) {
                dd.add(sum.divide(new BigDecimal(period), 16, RoundingMode.HALF_UP));
                counter = 0;
            }

            counter++;

        }

        childNode.set(serialName, objectMapper.valueToTree(dd));
    }

    private void timeDataController(ObjectNode childNode, int period, String serialName, JsonNode serialData) {

        int counter = 1;
        List<String> timeSerie = new ArrayList<>();
        List<Instant> instants = new ArrayList<>();

        for (final JsonNode data : serialData) {
            String iso8601Date = data.toString().replace("\"", "");
            Instant instant = Instant.parse(iso8601Date);
            instants.add(instant);

            if (counter == period) {
                long epochMilliAvg = (long) instants.stream().mapToLong(Instant::toEpochMilli).average().getAsDouble();
                Instant epochMilli = Instant.ofEpochMilli(epochMilliAvg);
                timeSerie.add(epochMilli.toString());

                instants = new ArrayList<>();
                counter = 0;
            }

            counter++;
        }

        childNode.set(serialName, objectMapper.valueToTree(timeSerie));
    }

}
