package cl.intellisense.rdelgado.testproject.service;

import cl.intellisense.rdelgado.testproject.payload.PeriodDto;
import com.fasterxml.jackson.databind.node.ObjectNode;

public interface PeriodService {
    ObjectNode getPeriods(PeriodDto periodDto);
}
