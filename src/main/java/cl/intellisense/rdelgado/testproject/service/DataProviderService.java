package cl.intellisense.rdelgado.testproject.service;

import com.fasterxml.jackson.databind.JsonNode;

public interface DataProviderService {
    JsonNode convertToJsonNode(String dataset);

    String getDataset();
}
