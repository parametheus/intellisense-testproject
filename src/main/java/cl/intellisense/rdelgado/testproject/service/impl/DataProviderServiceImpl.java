package cl.intellisense.rdelgado.testproject.service.impl;

import cl.intellisense.rdelgado.testproject.exception.TestprojectException;
import cl.intellisense.rdelgado.testproject.service.DataProviderService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class DataProviderServiceImpl implements DataProviderService {

    @Autowired
    ObjectMapper objectMapper;

    @Value("${intellisense.dataprovider.url}")
    String intellisenseDataproviderUrl;

    public String getDataset() {

        SimpleClientHttpRequestFactory clientHttpRequestFactory = new SimpleClientHttpRequestFactory();
        clientHttpRequestFactory.setConnectTimeout(3000);
        clientHttpRequestFactory.setReadTimeout(3000);
        RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);

        String dataset = restTemplate.getForObject(intellisenseDataproviderUrl, String.class);

        if (dataset == null) {
            throw new TestprojectException(HttpStatus.NO_CONTENT, "Intellisense endpoint returned no data");
        }

        return dataset;
    }

    @Override
    public JsonNode convertToJsonNode(String dataset) {
        JsonNode jsonNode;

        try {
            jsonNode = objectMapper.readTree(dataset);
        } catch (JsonProcessingException e) {
            throw new TestprojectException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }

        return jsonNode;
    }
}
