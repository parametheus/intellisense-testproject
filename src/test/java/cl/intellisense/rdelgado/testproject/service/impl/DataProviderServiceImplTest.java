package cl.intellisense.rdelgado.testproject.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.TestPropertySource;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
@TestPropertySource("classpath:application.properties")
class DataProviderServiceImplTest {

    @InjectMocks
    DataProviderServiceImpl dataProviderService;

    @Spy
    ObjectMapper objectMapper = new ObjectMapper();

    @Value("${intellisense.dataprovider.url}")
    private String intellisenseDataproviderUrl;

    String outputString;

    @BeforeEach
    void setUp() throws IOException {
        File file = new File(this.getClass().getClassLoader().getResource("test-dataset/intellisense_endpoint_testdata1.json").getFile());
        InputStream targetStream = new FileInputStream(file);

        byte[] fileData = FileCopyUtils.copyToByteArray(targetStream);
        outputString = new String(fileData);
    }

    @Test
    void itShouldBeNumericValueAtFirstElementOfFirstGroupOfData() {
        assertEquals("4.7583250633888765", dataProviderService.convertToJsonNode(outputString).get("660").get("3000").get(0).toString());
    }

}