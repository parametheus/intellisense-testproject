package cl.intellisense.rdelgado.testproject.constraints;

import cl.intellisense.rdelgado.testproject.payload.PeriodDto;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintValidatorContext;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class OneOfValidatorTest {

    private OneOf oneOf = mock(OneOf.class);
    private ConstraintValidatorContext constraintValidatorContext = mock(ConstraintValidatorContext.class);

    @Test
    public void itShouldBeValidWhenValueIsWithinDesiredValues() {

        int[] data = {10, 30, 60};

        when(oneOf.value()).thenReturn(data);

        OneOfValidator oneOfValidator = new OneOfValidator();
        oneOfValidator.initialize(oneOf);

        PeriodDto periodDto = new PeriodDto();
        periodDto.setPeriod(15);

        boolean result = oneOfValidator.isValid(periodDto.getPeriod(), constraintValidatorContext);

        assertFalse(result);
    }

}