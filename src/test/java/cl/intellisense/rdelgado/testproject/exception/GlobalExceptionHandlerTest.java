package cl.intellisense.rdelgado.testproject.exception;

import cl.intellisense.rdelgado.testproject.payload.ErrorDetails;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class GlobalExceptionHandlerTest {

    private GlobalExceptionHandler globalExceptionHandler = new GlobalExceptionHandler();
    MockHttpServletRequest servletRequest = new MockHttpServletRequest();

    @Test
    public void whenResourceNotFoundException_thenResourceNotFoundException() {
        ResourceNotFoundException resourceNotFoundException = new ResourceNotFoundException("resourceName", "fieldName", "fieldValue");

        servletRequest.setServerName("reference.intellisense.io");
        servletRequest.setRequestURI("/api/notfound");
        servletRequest.addParameter("parameter1", "value1");
        servletRequest.addParameter("parameter2", "value2");
        WebRequest webRequest = new ServletWebRequest(servletRequest);

        ResponseEntity<ErrorDetails> result = globalExceptionHandler.handleResourceNotFoundException(resourceNotFoundException, webRequest);
        assertNotNull(result);
    }
}